import React from "react";
import { Box } from "@mui/system";
import { Typography } from "@mui/material";
import imgS from "./1.svg";
import icon from "./2.svg";

const Better = () => {
  return (
    <Box display="flex" gap="20px">
      <Box position="relative" top="-45px">
        <img style={{ width: "760px" }} src={imgS} alt="sasas" />
      </Box>
      <Box display="flex" flexDirection="column" gap="10px" mt={6}>
        <Typography fontSize="17px" color="#1D92FF" fontWeight="500" pl="1px">
          Lorem ipsum dolor
        </Typography>
        <Typography fontWeight="600" width="200%" fontSize="40px" color=" #000000">
          Two Saas Are Better
          <br /> than one.
        </Typography>
        <Typography>
          One of the most fundamental ways to go about that is to create a memorable e-commerce
          slogan.
        </Typography>
        <Box display="flex" flexDirection="column" gap="20px">
          <Box display="flex" gap="10px">
            <img src={icon} alt="" />
            <Typography fontSize="14px" color="#01070D" sx={{ opacity: "0.5" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            </Typography>
          </Box>
          <Box display="flex" gap="10px">
            <img src={icon} alt="" />
            <Typography fontSize="14px" color="#01070D" sx={{ opacity: "0.5" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            </Typography>
          </Box>
          <Box display="flex" gap="10px">
            <img src={icon} alt="" />
            <Typography fontSize="14px" color="#01070D" sx={{ opacity: "0.5" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            </Typography>
          </Box>
          <Box display="flex" gap="10px">
            <img src={icon} alt="" />
            <Typography fontSize="14px" color="#01070D" sx={{ opacity: "0.5" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Better;
