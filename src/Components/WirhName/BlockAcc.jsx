import React from "react";
import { Box } from "@mui/system";
import { Button, Typography } from "@mui/material";
import CustomizedAccordions from "../Accordion/Accordion";
import FullScreenDialog from "../ButtonModal/Modal";
import SimpleSnackbar from "../Button/Button";
import imgS from "./Group 463.svg";

const BlockAcc = () => {
  return (
    <Box display={"flex"} gap="50px">
      <Box display="flex" flexDirection="column" gap="20px">
        <Typography fontSize="18px" color="#1D92FF" fontWeight="500" pl="1px">
          Lorem ipsum dolor
        </Typography>
        <Typography fontWeight="600" width="200%" fontSize="40px" color=" #000000">
          With a name like Saas,
          <br /> it has to be good.
        </Typography>
        <CustomizedAccordions />
        <Box display={"flex"} gap="23px" mt="10px">
          <FullScreenDialog />
          <SimpleSnackbar />
        </Box>
      </Box>
      <Box>
        <img style={{ position: "relative", top: "-40px" }} src={imgS} alt="" />
      </Box>
    </Box>
  );
};

export default BlockAcc;
