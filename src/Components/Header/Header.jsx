import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Slide from "@mui/material/Slide";
import logo from "./Subtract.svg";
import { Link } from "@mui/material";
import AccountMenu from "./AccountMenu";

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func,
};

export default function HideAppBar(props) {
  return (
    <React.Fragment>
      <CssBaseline />
      <HideOnScroll {...props}>
        <AppBar sx={{ bgcolor: "#002547" }}>
          <Container maxWidth="lg">
            <Box display={"flex"} justifyContent="space-between">
              <Toolbar display={"flex"}>
                <img style={{ width: "26px" }} src={logo} alt="" />
                <Typography variant="h6" pl={1}>
                  Base
                </Typography>
              </Toolbar>
              <Box
                display={{ xs: "none", sm: "none", md: "flex", lg: "flex" }}
                justifyContent="center"
                alignItems={"center"}
                gap="20px"
                pt="5px"
                color="#fff"
              >
                <Link color="#fff" href="#">
                  Home
                </Link>
                <Link color="#fff" href="#">
                  Features
                </Link>
                <Link color="#fff" href="#">
                  Testimonial
                </Link>
                <Link color="#fff" href="#">
                  Pricing
                </Link>
                <Link color="#fff" href="#">
                  CTA
                </Link>
              </Box>
              <AccountMenu />
            </Box>
          </Container>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
    </React.Fragment>
  );
}
