import React from "react";
import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const Cards = () => {
  return (
    <Box>
      <Box display={"flex"} gap="10px" flexDirection="column" alignItems="center">
        <Typography fontSize="40px" fontWeight="600">
          Keep Calm And Buy
        </Typography>
        <Typography>
          With lowering the price to even a slight extent one can achieve exponential growth.
        </Typography>
      </Box>
    </Box>
  );
};

export default Cards;
