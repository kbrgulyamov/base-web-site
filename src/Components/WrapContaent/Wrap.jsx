import React from "react";
import { Box } from "@mui/system";
import { Button, Typography } from "@mui/material";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "#fff",
  // border: "2px solid #1D92FF",
  boxShadow: 24,
  p: 9,
};
const Wrap = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <Box display={"flex"} gap="230px" pt="110px">
      <Box display="flex" flexDirection={"column"} gap="20px">
        <Typography fontSize="17px" color="#1D92FF" fontWeight="500" pl="1px">
          Lorem ipsum dolor
        </Typography>
        <Typography
          fontSize={{ xs: "35px", sm: "50px", lg: "60px" }}
          fontWeight="600"
          lineHeight="130%"
          color={"#fff"}
          width="900%"
        >
          Always The{" "}
          <span style={{ color: "#1D92FF", borderBottom: "5px solid #1D92FF" }}>Real</span>
          <br /> Thing, Tlways <span style={{ color: "#1D92FF" }}>Saas</span>
        </Typography>
        <Typography fontSize="18px" color="rgba(255, 255, 255, 0.7);" fontWeight="400">
          And That Number is growing every day.
          <br />
          That means{" "}
          <span style={{ color: "#fff", borderBottom: "2px solid #fff" }}>E-commerce</span> is
          thriving.
        </Typography>
        <Box display={"flex"} gap="20px">
          <Button
            sx={{
              bgcolor: "#fff",
              color: "#1D92FF",
              width: "205px",
              height: "48px",
              "&:hover": { backgroundColor: "#1D92FF", color: "#fff" },
            }}
          >
            Try For Free
          </Button>
          <Button
            onClick={handleOpen}
            sx={{
              bgcolor: "none",
              border: "1px solid #fff",
              color: "#fff",
              width: "205px",
              height: "48px",
            }}
          >
            Open modal
          </Button>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <Typography id="modal-modal-title" variant="h6" component="h2">
                Text in a modal
              </Typography>
              <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
              </Typography>
            </Box>
          </Modal>
        </Box>
      </Box>
      <Box position="relative" top="-65px">
        <img
          style={{ width: "600px", transform: "rotate(-16.64deg)" }}
          src="https://s3-alpha-sig.figma.com/img/9012/7fff/cbd6a875842a35e181d67c7f19e85140?Expires=1660521600&Signature=gjpx2CA2EVYxC7H92It2i4jcjx~JxMCTQp4A6e8HGkEg5QJJRdJY4IS0vmb22n~uQuhA2WYezlmE2lzBEPkUdppFQVNO7BQckywO-a5enlpNZ45VBsh~GbhuOlDR3wEkAxQkw9ImQ9UPU8KMfNH9h-QnWanMThpYyXF1zYw8k9Vua46m73pmYm~tXDV0CxEuDoGgjf9C2NMKgXCTtYDl6YjeeQATzMPRkJk-60JBNKffw2aTCmRWGjZz63iV4fm~2Q118km-VuH~AmIX5y0KgYB3v0TlXXDByNzAI8Uy-xuZe0Xr~MppNE0L2ZT8qnzCXQ4q0PRv3oyd-AFQP4~0Vw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
          alt=""
        />
      </Box>
    </Box>
  );
};

export default Wrap;
