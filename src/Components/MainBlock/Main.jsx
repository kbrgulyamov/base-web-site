import { Container } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import Better from "../Better/Better";
import Cards from "../Cards/Cards";
import HideAppBar from "../Header/Header";
import BasicSpeedDial from "../Speed/speed";
import BlockAcc from "../WirhName/BlockAcc";
import Wrap from "../WrapContaent/Wrap";

const Main = () => {
  return (
    <Box>
      <HideAppBar />
      <Box width="102%" height={560} bgcolor="#002547">
        <Container maxWidth="lg">
          <Box pl="20px">
            <Wrap />
          </Box>
          <Box position="relative" right="110px">
            <Better />
          </Box>
          <Box pl="22px" position="relative" top="-50px">
            <BlockAcc />
          </Box>
          <Box position="relative" top="-60px">
            <Cards />
          </Box>
        </Container>
      </Box>
    </Box>
  );
};

export default Main;
