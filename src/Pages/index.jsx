import React from "react";
import Main from "../Components/MainBlock/Main";
import BasicSpeedDial from "../Components/Speed/speed";

const Index = () => {
  return (
    <div>
      <Main />
      <BasicSpeedDial />
    </div>
  );
};

export default Index;
